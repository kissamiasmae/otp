import argparse
import array
import subprocess
import string

class Converter():

    def __init__(self, options):

        self.options = options
        self.list_lowerascii = list(string.ascii_lowercase)
        self.key='thisisthekey'


    def charToNum(self, char):
        return self.list_lowerascii.index(char)

    def encodeChar(self, character, index):
        cNum = ( self.charToNum(character) + self.charToNum(self.key[index%len(self.key)]) ) % 26
        return self.list_lowerascii[cNum]

    def decodeChar(self, character, index):
        cNum = ( self.charToNum(character) - self.charToNum(self.key[index%len(self.key)]) ) % 26
        return self.list_lowerascii[cNum]

    def encode(self, Text):
        encoded_text= ""
        index_key = 0
        for i in range(0, len(Text)):
            if Text[i].lower() in self.list_lowerascii:
                encoded_text += self.encodeChar(Text[i].lower(), index_key)
                index_key += 1
            else:
                encoded_text += Text[i]
        return encoded_text

    def decode(self, Text):
        decode_text = ""
        index_key = 0
        for i in range(0, len(Text)):
            if Text[i].lower() in self.list_lowerascii:
                decode_text += self.decodeChar(Text[i].lower(), index_key)
                index_key += 1
            else:
                decode_text += Text[i]
        return decode_text

    def run(self):

        if self.options.encode :
            text = self.options.encode
            encrypted_text = self.encode(text)
            print("encrypted_text .........", encrypted_text)

        elif self.options.decode:
            decoded_text = self.decode(self.options.decode)
            print("decoded text ...........", decoded_text)

        elif self.options.encodefile:
            all_text = ""
            for line in open(self.options.encodefile).readlines():
                all_text += line
            encrypted_text = self.encode(all_text)
            file_object  = open("encoded_file", "w") 
            file_object.write(encrypted_text)

        elif self.options.decodefile:
            all_text = ""
            for line in open(self.options.decodefile).readlines():
                all_text += line
            decrypted_text = self.decode(all_text)
            print("decoded_text file .........", decrypted_text)
            file_object  = open("decoded_file", "w") 
            file_object.write(decrypted_text)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--decode", help="decode text")
    parser.add_argument("-e", "--encode", help="text to be encryoted")
    parser.add_argument("-df", '--decodefile', help="text file to be decrypted")
    parser.add_argument("-ef", '--encodefile', help="text file to be encrypted")
    options = parser.parse_args()

    converter = Converter(options)
    converter.run()

main()
